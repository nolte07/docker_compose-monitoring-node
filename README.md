# Prometheus Exporter Collection

Docker Compose Project with a preconfigured set of Exporters.
The Exporter published the metrics ounder the [Exporter Default Ports](https://github.com/prometheus/prometheus/wiki/Default-port-allocations).

## Usage

```bash
docker-compose up -d
```

```bash
docker-compose down -v
```

## Node Exporter

Exports ECS Information like disk usage etc. You find more information at [prometheus/node_exporter](https://github.com/prometheus/node_exporter).

## Cadvisor

Exports Information about the Local Running Containers, more informations on [google/cadvisor)](https://github.com/google/cadvisor).

## Conntrack Exporter

Export Network Connection Information, like Open Connections form any hosts, see [hiveco/conntrack_exporter](https://github.com/hiveco/conntrack_exporter).
